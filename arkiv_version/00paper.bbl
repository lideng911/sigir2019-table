%%% -*-BibTeX-*-
%%% Do NOT edit. File created by BibTeX with style
%%% ACM-Reference-Format-Journals [18-Jan-2012].

\begin{thebibliography}{16}

%%% ====================================================================
%%% NOTE TO THE USER: you can override these defaults by providing
%%% customized versions of any of these macros before the \bibliography
%%% command.  Each of them MUST provide its own final punctuation,
%%% except for \shownote{}, \showDOI{}, and \showURL{}.  The latter two
%%% do not use final punctuation, in order to avoid confusing it with
%%% the Web address.
%%%
%%% To suppress output of a particular field, define its macro to expand
%%% to an empty string, or better, \unskip, like this:
%%%
%%% \newcommand{\showDOI}[1]{\unskip}   % LaTeX syntax
%%%
%%% \def \showDOI #1{\unskip}           % plain TeX syntax
%%%
%%% ====================================================================

\ifx \showCODEN    \undefined \def \showCODEN     #1{\unskip}     \fi
\ifx \showDOI      \undefined \def \showDOI       #1{#1}\fi
\ifx \showISBNx    \undefined \def \showISBNx     #1{\unskip}     \fi
\ifx \showISBNxiii \undefined \def \showISBNxiii  #1{\unskip}     \fi
\ifx \showISSN     \undefined \def \showISSN      #1{\unskip}     \fi
\ifx \showLCCN     \undefined \def \showLCCN      #1{\unskip}     \fi
\ifx \shownote     \undefined \def \shownote      #1{#1}          \fi
\ifx \showarticletitle \undefined \def \showarticletitle #1{#1}   \fi
\ifx \showURL      \undefined \def \showURL       {\relax}        \fi
% The following commands are used for tagged output and should be
% invisible to TeX
\providecommand\bibfield[2]{#2}
\providecommand\bibinfo[2]{#2}
\providecommand\natexlab[1]{#1}
\providecommand\showeprint[2][]{arXiv:#2}

\bibitem[\protect\citeauthoryear{Ahmad~Ahmadov}{Ahmad~Ahmadov}{2015}]%
        {Ahmadov:2015:NTE}
\bibfield{author}{\bibinfo{person}{Julian Eberius Wolfgang Lehner
  Robert~Wrembel Ahmad~Ahmadov, Maik~Thiele}.} \bibinfo{year}{2015}\natexlab{}.
\newblock \showarticletitle{Towards a Hybrid Imputation Approach Using Web
  Tables}. In \bibinfo{booktitle}{\emph{Proc. of BDC '15}}.
  \bibinfo{pages}{21--30}.
\newblock


\bibitem[\protect\citeauthoryear{Bhagavatula, Noraset, and Downey}{Bhagavatula
  et~al\mbox{.}}{2015}]%
        {Bhagavatula:2015:TEL}
\bibfield{author}{\bibinfo{person}{Chandra~Sekhar Bhagavatula},
  \bibinfo{person}{Thanapon Noraset}, {and} \bibinfo{person}{Doug Downey}.}
  \bibinfo{year}{2015}\natexlab{}.
\newblock \showarticletitle{TabEL: Entity Linking in Web Tables}. In
  \bibinfo{booktitle}{\emph{Proc. of ISWC '15}}. \bibinfo{pages}{425--441}.
\newblock


\bibitem[\protect\citeauthoryear{Cafarella, Halevy, and Khoussainova}{Cafarella
  et~al\mbox{.}}{2009}]%
        {Cafarella:2009:DIR}
\bibfield{author}{\bibinfo{person}{Michael~J. Cafarella}, \bibinfo{person}{Alon
  Halevy}, {and} \bibinfo{person}{Nodira Khoussainova}.}
  \bibinfo{year}{2009}\natexlab{}.
\newblock \showarticletitle{Data Integration for the Relational Web}.
\newblock \bibinfo{journal}{\emph{Proc. of VLDB Endow.}}  \bibinfo{volume}{2}
  (\bibinfo{year}{2009}), \bibinfo{pages}{1090--1101}.
\newblock


\bibitem[\protect\citeauthoryear{Das~Sarma, Fang, Gupta, Halevy, Lee, Wu, Xin,
  and Yu}{Das~Sarma et~al\mbox{.}}{2012}]%
        {DasSarma:2012:FRT}
\bibfield{author}{\bibinfo{person}{Anish Das~Sarma}, \bibinfo{person}{Lujun
  Fang}, \bibinfo{person}{Nitin Gupta}, \bibinfo{person}{Alon Halevy},
  \bibinfo{person}{Hongrae Lee}, \bibinfo{person}{Fei Wu},
  \bibinfo{person}{Reynold Xin}, {and} \bibinfo{person}{Cong Yu}.}
  \bibinfo{year}{2012}\natexlab{}.
\newblock \showarticletitle{Finding Related Tables}. In
  \bibinfo{booktitle}{\emph{Proc. of SIGMOD '12}}. \bibinfo{pages}{817--828}.
\newblock


\bibitem[\protect\citeauthoryear{Gentile, Ristoski, Eckel, Ritze, and
  Paulheim}{Gentile et~al\mbox{.}}{2017}]%
        {Anna:2017:EMW}
\bibfield{author}{\bibinfo{person}{Anna~Lisa Gentile}, \bibinfo{person}{Petar
  Ristoski}, \bibinfo{person}{Steffen Eckel}, \bibinfo{person}{Dominique
  Ritze}, {and} \bibinfo{person}{Heiko Paulheim}.}
  \bibinfo{year}{2017}\natexlab{}.
\newblock \showarticletitle{Entity Matching on Web Tables: a Table Embeddings
  approach for Blocking}. In \bibinfo{booktitle}{\emph{Proc. of EDBT '17}}.
  \bibinfo{pages}{510--513}.
\newblock


\bibitem[\protect\citeauthoryear{Ghasemi{-}Gol and Szekely}{Ghasemi{-}Gol and
  Szekely}{2018}]%
        {Majid:2018:TTV}
\bibfield{author}{\bibinfo{person}{Majid Ghasemi{-}Gol} {and}
  \bibinfo{person}{Pedro~A. Szekely}.} \bibinfo{year}{2018}\natexlab{}.
\newblock \showarticletitle{TabVec: Table Vectors for Classification of Web
  Tables}.
\newblock \bibinfo{journal}{\emph{CoRR}} (\bibinfo{year}{2018}).
\newblock


\bibitem[\protect\citeauthoryear{Mikolov, Sutskever, Chen, Corrado, and
  Dean}{Mikolov et~al\mbox{.}}{2013}]%
        {Mikolov:2013:DRW}
\bibfield{author}{\bibinfo{person}{Tomas Mikolov}, \bibinfo{person}{Ilya
  Sutskever}, \bibinfo{person}{Kai Chen}, \bibinfo{person}{Greg~S Corrado},
  {and} \bibinfo{person}{Jeff Dean}.} \bibinfo{year}{2013}\natexlab{}.
\newblock \showarticletitle{Distributed Representations of Words and Phrases
  and their Compositionality}.
\newblock In \bibinfo{booktitle}{\emph{Advances in Neural Information
  Processing Systems 26}}. \bibinfo{pages}{3111--3119}.
\newblock


\bibitem[\protect\citeauthoryear{Milne and Witten}{Milne and Witten}{2008}]%
        {Milne:2008:ELM}
\bibfield{author}{\bibinfo{person}{David Milne} {and} \bibinfo{person}{Ian~H.
  Witten}.} \bibinfo{year}{2008}\natexlab{}.
\newblock \showarticletitle{An Effective, Low-Cost Measure of Semantic
  Relatedness Obtained from Wikipedia Links}. In
  \bibinfo{booktitle}{\emph{Proc. of AAAI '08}}.
\newblock


\bibitem[\protect\citeauthoryear{Mohamed~Yakout and Chaudhuri}{Mohamed~Yakout
  and Chaudhuri}{2012}]%
        {Yakout:2012:NTE}
\bibfield{author}{\bibinfo{person}{Kaushik~Chakrabarti Mohamed~Yakout,
  Kris~Ganjam} {and} \bibinfo{person}{Surajit Chaudhuri}.}
  \bibinfo{year}{2012}\natexlab{}.
\newblock \showarticletitle{InfoGather: entity augmentation and attribute
  discovery by holistic matching with Web tables}. In
  \bibinfo{booktitle}{\emph{Proc. of SIGMOD '12}}. \bibinfo{pages}{97--108}.
\newblock


\bibitem[\protect\citeauthoryear{Pimplikar and Sarawagi}{Pimplikar and
  Sarawagi}{2012}]%
        {Pimplikar:2012:ATQ}
\bibfield{author}{\bibinfo{person}{Rakesh Pimplikar} {and}
  \bibinfo{person}{Sunita Sarawagi}.} \bibinfo{year}{2012}\natexlab{}.
\newblock \showarticletitle{Answering Table Queries on the Web Using Column
  Keywords}.
\newblock \bibinfo{journal}{\emph{Proc. of VLDB Endow.}}  \bibinfo{volume}{5}
  (\bibinfo{year}{2012}), \bibinfo{pages}{908--919}.
\newblock


\bibitem[\protect\citeauthoryear{Ristoski and Paulheim}{Ristoski and
  Paulheim}{2016}]%
        {Ristoski:2016:RGE}
\bibfield{author}{\bibinfo{person}{Petar Ristoski} {and} \bibinfo{person}{Heiko
  Paulheim}.} \bibinfo{year}{2016}\natexlab{}.
\newblock \showarticletitle{{RDF2vec}: {RDF} Graph Embeddings for Data Mining}.
  In \bibinfo{booktitle}{\emph{Proc. of ISWC '16}}. \bibinfo{pages}{498--514}.
\newblock


\bibitem[\protect\citeauthoryear{Yoones A.~Sekhavat}{Yoones
  A.~Sekhavat}{2014}]%
        {Sekhavat:2014:KBE}
\bibfield{author}{\bibinfo{person}{Denilson Barbosa Paolo~Merialdo Yoones
  A.~Sekhavat, Francesco di~Paolo}.} \bibinfo{year}{2014}\natexlab{}.
\newblock \showarticletitle{Knowledge Base Augmentation using Tabular Data}. In
  \bibinfo{booktitle}{\emph{Proc. of LDOW '14}}.
\newblock


\bibitem[\protect\citeauthoryear{Zhang}{Zhang}{2018}]%
        {Zhang:2018:SES}
\bibfield{author}{\bibinfo{person}{Shuo Zhang}.}
  \bibinfo{year}{2018}\natexlab{}.
\newblock \showarticletitle{SmartTable: Equipping Spreadsheets with Intelligent
  Assistance Functionalities}. In \bibinfo{booktitle}{\emph{Proc. of SIGIR
  '18}}. \bibinfo{pages}{1447--1447}.
\newblock


\bibitem[\protect\citeauthoryear{Zhang and Balog}{Zhang and Balog}{2017}]%
        {Zhang:2017:ESA}
\bibfield{author}{\bibinfo{person}{Shuo Zhang} {and} \bibinfo{person}{Krisztian
  Balog}.} \bibinfo{year}{2017}\natexlab{}.
\newblock \showarticletitle{EntiTables: Smart Assistance for Entity-Focused
  Tables}. In \bibinfo{booktitle}{\emph{Proc. of SIGIR '17}}.
  \bibinfo{pages}{255--264}.
\newblock


\bibitem[\protect\citeauthoryear{Zhang and Balog}{Zhang and Balog}{2018a}]%
        {Zhang:2018:AHT}
\bibfield{author}{\bibinfo{person}{Shuo Zhang} {and} \bibinfo{person}{Krisztian
  Balog}.} \bibinfo{year}{2018}\natexlab{a}.
\newblock \showarticletitle{Ad Hoc Table Retrieval using Semantic Similarity}.
  In \bibinfo{booktitle}{\emph{Proc. of WWW '18}}. \bibinfo{pages}{1553--1562}.
\newblock


\bibitem[\protect\citeauthoryear{Zhang and Balog}{Zhang and Balog}{2018b}]%
        {Zhang:2018:OTG}
\bibfield{author}{\bibinfo{person}{Shuo Zhang} {and} \bibinfo{person}{Krisztian
  Balog}.} \bibinfo{year}{2018}\natexlab{b}.
\newblock \showarticletitle{On-the-fly Table Generation}. In
  \bibinfo{booktitle}{\emph{Proc. of SIGIR '18}}. \bibinfo{pages}{595--604}.
\newblock


\end{thebibliography}
